# PushTest - A simple Python script to test Android push notifications
# implemented with GCM.
# Sends a simple message via GCM over HTTP to the client with the registration ID provided. (See https://developer.android.com/google/gcm/http.html)
# Uses Tkinter to create a basic GUI
# Author - Joe Alam - 21/04/2015
import urllib2
import Tkinter
import json

# Set these specific to your project.
# API Key is the key generated in the Google Developer Console console.developers.google.com
# Client ID is the registered ID of the device you wish to send the message to
# Message is the context of the message string
g_APIKey = "Your-API-Key"
g_ClientID = "Your-Client-ID"
g_Message = "Sample Message"

# SendNotification makes the HTTP request
def SendNotification(message, apikey, clientid):
    url = "http://android.googleapis.com/gcm/send"
    headers = { 'Authorization':  'key=' + apikey,
                'Content-Type': 'application/json' }

    # If required, customise the data sent for the message
    data = "{\
                \"data\": { \"message\": \"" + message + "\" },\
                \"registration_ids\":[\"" + clientid + "\"]\
             }"

    # Create the request
    request = urllib2.Request(url, data, headers)

    # Do the request
    print "Making request to " + url
    try:
        response = urllib2.urlopen(request)
        output = response.read()

        # Print the full response
        print "Response: "
        print output

        # Parse the response and determine success/failure
        responseData = json.loads(output)
        success = (responseData["success"] == 1)

        return success
        
    except urllib2.HTTPError as e:
        print "HTTP Connection Failed - " + str(e)
        return False
    except:
        print "Unknown error"
        return False

#######################################################################
# Tkinter UI Code Below Here
# Creates a basic GUI

class BasicMessageSender(Tkinter.Tk):
    def __init__(self,parent):
        Tkinter.Tk.__init__(self,parent)
        self.parent = parent
        self.geometry("500x120")
        self.initialize()

    def initialize(self):
        self.grid()

        row = 0

        # API Key
        label = Tkinter.Label(self, anchor="w",fg="black", text="API Key")
        label.grid(column=0,row=row,sticky='E')
        self.entryAPIKey = Tkinter.Entry(self)
        self.entryAPIKey.grid(column=1,row=row,sticky='WE')
        self.entryAPIKey.insert(0, g_APIKey)
        row += 1

        # Client ID
        label = Tkinter.Label(self, anchor="w",fg="black", text="Client ID")
        label.grid(column=0,row=row,sticky='E')
        self.entryClientID = Tkinter.Entry(self)
        self.entryClientID.grid(column=1,row=row,sticky='WE')
        self.entryClientID.insert(0, g_ClientID)
        row += 1

        # Message
        label = Tkinter.Label(self, anchor="w",fg="black", text="Message")
        label.grid(column=0,row=row,sticky='E')
        self.entryMessage = Tkinter.Entry(self)
        self.entryMessage.grid(column=1,row=row,sticky=Tkinter.E+Tkinter.W)
        self.entryMessage.insert(0, g_Message)
        row += 1

        # Instruction Label
        self.instructionVar = Tkinter.StringVar()
        self.instructionVar.set("Click")
        label = Tkinter.Label(self, anchor="w",fg="black", textvariable=self.instructionVar)
        label.grid(column=0,row=row,sticky='W',columnspan=2)

        # Send Button
        button = Tkinter.Button(self,text=u"Send",command=self.OnSendButtonClick)
        button.grid(column=1,row=row,sticky="E")

        # Ensure the grid is sized to fit the frame
        self.grid_columnconfigure(1, weight=1)

    def OnSendButtonClick(self):
        apikey = self.entryAPIKey.get()
        clientid = self.entryClientID.get()
        msg = self.entryMessage.get()

        print "Sending message..."
        success = SendNotification(msg, apikey, clientid)
        if (success):
            self.instructionVar.set("Success!")
        else:
            self.instructionVar.set("Failed. See print output for full response.")

if __name__ == "__main__":
    app = BasicMessageSender(None)
    app.title('GCM Push Notification Tester')
    app.mainloop()