# PushTest Readme
A simple Python script to test Android push notifications that are implemented via GCM.

* Uses urllib2 to make HTTP requests to send the notifications.
* Uses Tkinter to create a simple GUI for the required parameters

Tested with Python 2.7.6 on OSX 10.10.2